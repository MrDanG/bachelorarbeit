Zur Bearbeitung des virtuellen Labors benötigen Sie Docker.
Falls Sie Docker nicht installiert haben, folgen sie den Anleitungen unter: 
Windows:	https://docs.docker.com/desktop/install/windows-install/
Linux:		https://docs.docker.com/engine/install/ubuntu/
Mac:		https://docs.docker.com/desktop/install/mac-install/


Die folgenden befehle werden in der Kommandozeile ausgeführt.
Die Docker-Images des virtuellen Labors stehen als TAR-Datei zur Verfügung.
Um die Images nutzen zu können, muss die TAR-Datei entpackt werden.

Entpacken/Laden des virtuellen Labors:
	Führen Sie folgenden Befehl in dem Verzeichnis aus, in dem sich die Datei mit
	dem Namen "virtual_laboratory.tar" befindet. Dadurch werden die Docker-Images
	des virtuellen Labors geladen:
	>docker load -i virtual_laboratory.tar

Jetzt sollten insgesamt neun Docker-Images die alle mit "virtual_laboratory_" beginnen vorhanden sein.
Als nächstes wird der Docker-Container in dem die Aufgaben zum virtuellen Labor
bearbeitet werden gestartet.

Starten des Containers zur Bearbeitung der Aufgaben:
	Zum Starten des Containers der die Aufgaben des virtuellen Labors beinhaltet,
	geben Sie folgenden Befehl ein:
	>docker run -t -i virtual_laboratory_exercises:base /bin/bash

	Es sollte sich eine bash zum Container geöffnet haben.
	Sollte das Skript in dem die Aufgaben bearbeitet werden nicht automatisch gestartet werden,
	geben Sie folgenden Befehl ein um es zu starten:
	>python virtual_laboratory.py

Für die Bearbeitung der ersten Aufgaben müssen keine weiteren Docker-Container gestartet
werden.

--------------------------------------------------1--------------------------------------------------

Bei einer späteren Aufgabe werden Sie dazu aufgefordert das Labornetzwerk zu starten.
Dazu navigieren Sie in das Verzeichnis in dem sich die docker-compose Datei des virtuellen
Labors befindet oder geben den Pfad mithilfe des "-f" Parameters an 

Starten des virtuellen Labors:
	Führen Sie folgenden Befehl in dem Verzeichnis aus, in dem sich die docker-compose
	Datei des virtuellen Labors befindet:
	>docker-compose up -d
	
	Optional wenn nicht in selbem Verzeichnis:
	>docker-compose -f <path_to_file/docker-compose.yml> up -d

Sollten Sie das Labornetzwerk beenden wollen, geben Sie folgenden Befehl ein:

Beenden des virtuellen Labors:
	Führen Sie folgenden Befehl in dem Verzeichnis aus, in dem sich die docker-compose
	Datei des virtuellen Labors befindet:
	>docker-compose stop
	
	Optional wenn nicht in selbem Verzeichnis:
	>docker-compose -f <path_to_file/docker-compose.yml> stop

Als nächstes öffnen Sie eine bash zum Angreifer-System. Von diesem System aus wird
das Labornetzwerk ausgekundschaftet.

Öffnen eine bash zum Angreifer-System:
	>docker exec -i -t intruder bash

Über die bash können Sie das Angreifer-System steuern und das Labornetzwerk
auskundschaften. 
Prüfen Sie, ob Sie mit dem richtigen System verbunden sind. Geben Sie dazu "ip address" ein.
Die IP-Adresse sollte 192.168.1.100 lauten
Sollten Sie die bash zum Angreifer-System schließen wollen, geben Sie "exit" ein
oder schließen Sie das Fenster.
Die folgenden Aufgaben werden durch den Einsatz dieses Systems bearbeitet.

Empfehlung:
Benutzen Sie einen Texteditor, in dem Sie die Lösungen vorformulieren.
Viele der Aufgaben bauen aufeinander auf und eine Lösung entspricht einer vorherigen mit zusätzlichen Informationen.
Außerdem können falsche/fehlende Zeichen einfach angepasst und die Lösung erneut eingegeben (copy & paste) werden. 

--------------------------------------------------2--------------------------------------------------

Bei einer späteren Aufgabe soll passives Network Information Gathering betrieben werden.
Dazu wird eine bash zu dem Angreifer-System für passives scannen benötigt.
Außerdem wird die Bezeichnung des Bridge-Interfaces des einen docker-bridge Netzwerks
benötigt.
Um die Bezeichnung des Bridge-Interfaces zu ermitteln, wird die NETWORK ID des
internen Netzwerks des Labornetzwerks benötigt. Lassen Sie sich dazu alle Docker Netzwerke
anzeigen.

Anzeigen aller docker Netzwerke:
	>docker network ls

Eines der Netzwerke sollte den Namen "virtual_laboratory_network_internal" tragen.
Dessen NETWORK ID wird benötigt. Sie sollte beispielsweise so aussehen: 792c7c95230c
Die Bezeichnung des Bridge-Interfaces setzt sich folgendermaßen zusammen:
br-NETWORK ID. Also im Falle der obigen NETWORK ID: br-792c7c95230c
Bei Ihnen wird die NETWORK ID anders aussehen aber der Name des Bridge-Interfaces setzt
genauso zusammen. Merken Sie sich die Bezeichnung des Bridge-Interfaces.
Öffnen Sie als nächstes eine bash zum Angreifer-System das sich im Hostnetzwerk befindet.
Dies ist das Angreifer-System für passives scannen.

Öffnen einer bash zum Angreifer-System:
	>docker exec -i -t intruder_host bash

Nachdem das passive Network Information Gathering abgeschlossen ist, kann die bash
zum Angreifer-System im Hostnetzwerk geschlossen werden.
Geben Sie dazu "exit" ein oder schließen Sie das Fenster.

--------------------------------------------------3--------------------------------------------------

Als nächstes wird das Angreifer-System, das zuvor genutzt wurde, in ein anderes Netzwerk
versetzt. Das System wird in das Zielnetzwerk versetzt und das Netzwerk erneut von
innen gescannt.
Dazu wird die Verbindung zum aktuellen Netzwerk geschlossen und eine neue Verbindung
zum internen Netzwerk aufgebaut.
Im Moment befindet sich das Angreifer-System im Netzwerk "virtual_laboratory_network_external"
und soll in das Netzwerk "virtual_laboratory_network_internal" versetzt werden.
Die IP-Adresse des Systems (192.168.1.100) wird bezüglich des Subnetzes zu
192.168.10.100 verändert und muss bei der Verbindung zum neuen Netzwerk angegeben werden.

Verbindung zu einem Netzwerk schließen:
	>docker network disconnect virtual_laboratory_network_external intruder

Verbindung zu einem Netzwerk öffnen:
	>docker network connect --ip 192.168.10.100 virtual_laboratory_network_internal intruder

Damit ist das Angreifer-System in das interne Netzwerk versetzt und die folgende
Aufgabe kann bearbeitet werden.
Öffenen Sie eine bash zum Angreifer-System.

Öffnen einer bash zum Angreifer-System:
	>docker exec -i -t intruder bash

Prüfen Sie, ob das System erfolgreich versetzt wurde. Geben Sie dazu "ip address" ein.
Die IP-Adresse sollte 192.168.10.100 lauten

--------------------------------------------------4--------------------------------------------------

Um das virtuelle Labor zu beenden, geben Sie folgenden Befehl ein:

Beenden des virtuellen Labors:
	Führen Sie folgenden Befehl in dem Verzeichnis aus, in dem sich die docker-compose
	Datei des virtuellen Labors befindet:
	>docker-compose stop
	
	Optional wenn nicht in selbem Verzeichnis:
	>docker-compose -f <path_to_file/docker-compose.yml> stop

--------------------------------------------------5--------------------------------------------------

Wenn Sie das virtuelle Labor neu starten, müssen Sie das Angreifer-System wieder 
in das Netzwerk "virtual_laboratory_network_external" versetzen (IP-Adresse: 192.168.1.100)
Oder Sie starten das virtuelle Labor neu:

Beenden des virtuellen Labors:
	Führen Sie folgenden Befehl in dem Verzeichnis aus, in dem sich die docker-compose
	Datei des virtuellen Labors befindet:
	>docker-compose stop
	
	Optional wenn nicht in selbem Verzeichnis:
	>docker-compose -f <path_to_file/docker-compose.yml> stop

Starten des virtuellen Labors:
	Führen Sie folgenden Befehl in dem Verzeichnis aus, in dem sich die docker-compose
	Datei des virtuellen Labors befindet:
	>docker-compose up -d
	
	Optional wenn nicht in selbem Verzeichnis:
	>docker-compose -f <path_to_file/docker-compose.yml> up -d