Um die Aufgaben des virtuellen Labors anzupassen, nehmen Sie die Anpassungen in der 
Datei "virtual_laboratory_exercises.xml" vor.

Anschließend muss docker Image neu gebaut werden.
Löschen Sie zuerst das alte Image, es sollte den Namen "virtual_laboratory_exercises:base" tragen.
Geben Sie dazu folgenden Befehl ein:
	>docker image rm virtual_laboratory_exercises:base

Jetzt kann das Image neu gebaut werden.
Navigieren Sie in das Verzeichnis, in dem sich die Dateien zu dem Aufgaben-Image befinden.
In dem Verzeichnis befindet sich eine Datei mit dem Namen "Dockerfile".
Geben Sie in dem Verzeichnis folgenden Befehl ein:
	>docker build -t virtual_laboratory_exercises:base .

Es wird ein neues Image, ebenfalls mit dem Namen "virtual_laboratory_exercises:base", gebaut.