import virtual_laboratory_read_load_save
import virtual_laboratory_logic


def main():
    vl = virtual_laboratory_read_load_save.read_virtual_lab_xml()
    virtual_laboratory_logic.execute_virtual_lab_v2(vl)


main()
