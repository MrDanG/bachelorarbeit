import os.path
import xml.etree.ElementTree as ET
import virtual_laboratory_structure
import virtual_laboratory_statistic
import pickle


def get_element_text_by_tag(parent_element, tag):
    if parent_element.find(tag) is not None:
        return parent_element.find(tag).text
    else:
        return None


def read_virtual_lab_xml():
    # path = pathlib.Path('../')
    # file = path / 'virtual_laboratory_exercises.xml'
    # lab_tree = ET.parse(file)
    lab_tree = ET.parse('virtual_laboratory_exercises.xml')
    vl = virtual_laboratory_structure.VirtualLab()

    # read general texts from xml
    vl.set_general_texts(get_element_text_by_tag(lab_tree, 'virtual_lab_welcome_text'),
                         get_element_text_by_tag(lab_tree, 'virtual_lab_help_text'),
                         get_element_text_by_tag(lab_tree, 'virtual_lab_error_text'),
                         get_element_text_by_tag(lab_tree, 'virtual_lab_hint_solution_text'),
                         get_element_text_by_tag(lab_tree, 'virtual_lab_correct_solution_text'))

    # read topics from xml
    topics = lab_tree.findall('topic')
    for topic_element in topics:
        # read general topic information from xml
        topic = virtual_laboratory_structure.Topic(get_element_text_by_tag(topic_element, 'id'),
                                            get_element_text_by_tag(topic_element, 'title'),
                                            get_element_text_by_tag(topic_element, 'text'),
                                            get_element_text_by_tag(topic_element, 'end_text'))
        vl.add_sub_element(topic)

        # read sub topics from xml
        sub_topics = topic_element.findall('sub_topic')
        for sub_topic_element in sub_topics:
            # read general sub topic information from xml
            sub_topic = virtual_laboratory_structure.SubTopic(get_element_text_by_tag(sub_topic_element, 'id'),
                                                       get_element_text_by_tag(sub_topic_element, 'title'),
                                                       get_element_text_by_tag(sub_topic_element, 'text'),
                                                       get_element_text_by_tag(sub_topic_element, 'end_text'))
            topic.add_sub_element(sub_topic)

            # read tasks from xml
            tasks = sub_topic_element.findall('task')
            for task_element in tasks:
                # read task information form xml
                task = virtual_laboratory_structure.Task(get_element_text_by_tag(task_element, 'id'),
                                                  get_element_text_by_tag(task_element, 'title'),
                                                  get_element_text_by_tag(task_element, 'text'),
                                                  get_element_text_by_tag(task_element, 'exercise'),
                                                  get_element_text_by_tag(task_element, 'solution'),
                                                  get_element_text_by_tag(task_element, 'hint_1'),
                                                  get_element_text_by_tag(task_element, 'hint_2'),
                                                  get_element_text_by_tag(task_element, 'end_text'))
                sub_topic.add_sub_element(task)
    return vl


def save_virtual_lab(virtual_lab_state):
    with open('virtual_lab.pk1', 'wb+') as file:
        if virtual_lab_state[0]:
            pickle.dump(virtual_lab_state[0], file, pickle.HIGHEST_PROTOCOL)
        if virtual_lab_state[1]:
            pickle.dump(virtual_lab_state[1], file, pickle.HIGHEST_PROTOCOL)
        if virtual_lab_state[2]:
            pickle.dump(virtual_lab_state[2], file, pickle.HIGHEST_PROTOCOL)
        statistic = virtual_laboratory_statistic.get_virtual_lab_statistics_to_save()
        if statistic:
            for statistic_element in statistic:
                pickle.dump(statistic_element, file, pickle.HIGHEST_PROTOCOL)


def load_virtual_lab():
    topic = None
    sub_topic = None
    task = None
    statistic = []
    if os.path.isfile('virtual_lab.pk1'):
        with open('virtual_lab.pk1', 'rb') as file:
            while True:
                try:
                    tmp_loaded_element = pickle.load(file)
                    if tmp_loaded_element:

                        if isinstance(tmp_loaded_element, virtual_laboratory_structure.Topic) or \
                                tmp_loaded_element == 'LAST_TOPIC_COMPLETE_SHOW_END_TEXT':
                            topic = tmp_loaded_element
                        elif isinstance(tmp_loaded_element, virtual_laboratory_structure.SubTopic) or \
                                tmp_loaded_element == 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT':
                            sub_topic = tmp_loaded_element
                        elif isinstance(tmp_loaded_element, virtual_laboratory_structure.Task) or \
                                tmp_loaded_element == 'LAST_TASK_COMPLETE_SHOW_END_TEXT':
                            task = tmp_loaded_element
                        elif isinstance(tmp_loaded_element, virtual_laboratory_statistic.StatisticTopic):
                            statistic.append(tmp_loaded_element)
                    else:
                        break
                except(EOFError, pickle.UnpicklingError):
                    break
            if statistic:
                virtual_laboratory_statistic.set_virtual_lab_statistics_from_save(statistic)
    return topic, sub_topic, task
