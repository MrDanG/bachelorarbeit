import sys
import virtual_laboratory_structure
import virtual_laboratory_statistic
import virtual_laboratory_read_load_save

virtual_lab_reference: virtual_laboratory_structure.VirtualLab = None
current_topic: virtual_laboratory_structure.Topic = None
current_topic_complete = False
current_sub_topic: virtual_laboratory_structure.SubTopic = None
current_sub_topic_complete = False
current_task: virtual_laboratory_structure.Task = None
current_task_complete = False

VIRTUAL_LAB_YES = 'yes'
VIRTUAL_LAB_NO = 'no'
VIRTUAL_LAB_SAVE = 'Do you want to save the state of the virtual laboratory? (yes/no)'
VIRTUAL_LAB_RESUME = 'Do you want to resume the virtual laboratory? (yes/no)'
VIRTUAL_LAB_RESUME_ERROR = 'There is no saved virtual laboratory!\n' \
                           'The virtual laboratory starts from the beginning'
VIRTUAL_LAB_RESTART = 'Do you want to start the virtual laboratory over? (yes/no)'


def input_virtual_lab_text(text):
    if text is not None:
        return input(text + "\n").strip()
    else:
        return ''


def print_virtual_lab_text(text):
    if text is not None:
        print(text + "\n")
    else:
        return


def is_input_solution(user_input, solution):
    user_input = user_input.replace(' ', '')
    solution = solution.replace(' ', '')
    user_input = user_input.lower()
    solution = solution.lower()
    if '[' in solution and ']' in solution:
        while solution.find('[') != -1:
            start_index = solution.find('[')
            end_index = solution.find(']')
            variable_solution = solution[start_index + 1:end_index]
            variable_solution = variable_solution.split(';')
            is_variable_solution_in_user_input = False
            for var in variable_solution:
                var_length = len(var)
                variable_user_input = user_input[start_index:(start_index + var_length)]
                if variable_user_input == var:
                    is_variable_solution_in_user_input = True
                    solution = solution[0: start_index:] + var + solution[end_index + 1::]
                    break
            if not is_variable_solution_in_user_input:
                return False
        solution = solution.replace(',', '')
        solution = solution.replace(';', '')
        user_input = user_input.replace(',', '')
        user_input = user_input.replace(';', '')
        if user_input == solution:
            return True
        else:
            return False
    elif '$' in solution:
        if len(solution) < len(user_input):
            if len(solution) + 1 < len(user_input):
                return False
            elif user_input[-1] != ';':
                return False
        solution = solution.replace(',', '')
        solution = solution.replace(';', '')
        user_input = user_input.replace(',', '')
        user_input = user_input.replace(';', '')
        for (tmp_solution, tmp_user_input) in zip(solution, user_input):
            if tmp_solution == '$':
                continue
            if tmp_solution != tmp_user_input:
                return False
        return True
    else:
        solution = solution.replace(',', '')
        solution = solution.replace(';', '')
        user_input = user_input.replace(',', '')
        user_input = user_input.replace(';', '')
        if user_input == solution:
            return True
        else:
            return False


def set_exit_flags(current_element, complete):
    if isinstance(current_element, virtual_laboratory_structure.Topic):
        global current_topic_complete
        current_topic_complete = complete
    if isinstance(current_element, virtual_laboratory_structure.SubTopic):
        global current_sub_topic_complete
        current_sub_topic_complete = complete
    if isinstance(current_element, virtual_laboratory_structure.Task):
        global current_task_complete
        current_task_complete = complete


def set_global_reference(current_element):
    if isinstance(current_element, virtual_laboratory_structure.Topic):
        global current_topic
        current_topic = current_element
    if isinstance(current_element, virtual_laboratory_structure.SubTopic):
        global current_sub_topic
        current_sub_topic = current_element
    if isinstance(current_element, virtual_laboratory_structure.Task):
        global current_task
        current_task = current_element


def is_virtual_lab_structure_element_empty(element):
    if not element.get_title() and \
            not element.get_info_text() and \
            not element.get_end_text():
        if isinstance(element, virtual_laboratory_structure.Topic) or \
                isinstance(element, virtual_laboratory_structure.SubTopic):
            if not element.get_next_sub_element(None):
                return True
            else:
                return False
        else:
            return True
    return False


def exit_virtual_lab():
    user_input = input_virtual_lab_text(VIRTUAL_LAB_SAVE)
    if user_input == VIRTUAL_LAB_YES:
        topic_to_save = None
        sub_topic_to_save = None
        task_to_save = None
        if current_topic_complete:
            if not virtual_lab_reference.get_next_sub_element(current_topic):
                topic_to_save = 'LAST_TOPIC_COMPLETE_SHOW_END_TEXT'
                sub_topic_to_save = 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT'
                task_to_save = 'LAST_TASK_COMPLETE_SHOW_END_TEXT'
                virtual_laboratory_read_load_save.save_virtual_lab((topic_to_save, sub_topic_to_save, task_to_save))
                sys.exit()
            else:
                tmp_topic_to_save = virtual_lab_reference.get_next_sub_element(current_topic)
                while tmp_topic_to_save and is_virtual_lab_structure_element_empty(tmp_topic_to_save):
                    tmp_topic_to_save = virtual_lab_reference.get_next_sub_element(tmp_topic_to_save)
                if not tmp_topic_to_save:
                    topic_to_save = 'LAST_TOPIC_COMPLETE_SHOW_END_TEXT'
                else:
                    topic_to_save = tmp_topic_to_save
                    virtual_laboratory_statistic.handle_statistic(topic_to_save)
        else:
            topic_to_save = current_topic
        if current_sub_topic_complete:
            if current_topic_complete:
                if topic_to_save:
                    tmp_sub_topic_to_save = topic_to_save.get_next_sub_element(None)
                    while tmp_sub_topic_to_save and is_virtual_lab_structure_element_empty(tmp_sub_topic_to_save):
                        tmp_sub_topic_to_save = topic_to_save.get_next_sub_element(tmp_sub_topic_to_save)
                    if not tmp_sub_topic_to_save:
                        sub_topic_to_save = 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT'
                    else:
                        sub_topic_to_save = tmp_sub_topic_to_save
                        virtual_laboratory_statistic.handle_statistic(sub_topic_to_save)
                else:
                    sub_topic_to_save = 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT'
            elif not current_topic.get_next_sub_element(current_sub_topic):
                sub_topic_to_save = 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT'
            else:
                tmp_sub_topic_to_save = current_topic.get_next_sub_element(current_sub_topic)
                while tmp_sub_topic_to_save and is_virtual_lab_structure_element_empty(tmp_sub_topic_to_save):
                    tmp_sub_topic_to_save = current_topic.get_next_sub_element(tmp_sub_topic_to_save)
                if not tmp_sub_topic_to_save:
                    sub_topic_to_save = 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT'
                else:
                    sub_topic_to_save = tmp_sub_topic_to_save
                    virtual_laboratory_statistic.handle_statistic(sub_topic_to_save)
        else:
            sub_topic_to_save = current_sub_topic
        if current_task_complete:
            if current_sub_topic_complete or not current_sub_topic.get_next_sub_element(current_task):
                if (current_topic_complete and sub_topic_to_save != 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT') or \
                        sub_topic_to_save != 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT' and \
                        sub_topic_to_save.get_next_sub_element(current_task):
                    tmp_task_to_save = sub_topic_to_save.get_next_sub_element(current_task)
                    while tmp_task_to_save and is_virtual_lab_structure_element_empty(tmp_task_to_save):
                        tmp_task_to_save = sub_topic_to_save.get_next_sub_element(tmp_task_to_save)
                    if not tmp_task_to_save:
                        task_to_save = 'LAST_TASK_COMPLETE_SHOW_END_TEXT'
                    else:
                        task_to_save = tmp_task_to_save
                        virtual_laboratory_statistic.handle_statistic(task_to_save)
                else:
                    task_to_save = 'LAST_TASK_COMPLETE_SHOW_END_TEXT'
            else:
                tmp_task_to_save = current_sub_topic.get_next_sub_element(current_task)
                while tmp_task_to_save and is_virtual_lab_structure_element_empty(tmp_task_to_save):
                    tmp_task_to_save = current_sub_topic.get_next_sub_element(tmp_task_to_save)
                if not tmp_task_to_save:
                    task_to_save = 'LAST_TASK_COMPLETE_SHOW_END_TEXT'
                else:
                    task_to_save = tmp_task_to_save
                    virtual_laboratory_statistic.handle_statistic(task_to_save)
        else:
            task_to_save = current_task

        virtual_laboratory_read_load_save.save_virtual_lab((topic_to_save, sub_topic_to_save, task_to_save))
        sys.exit()
    elif user_input == VIRTUAL_LAB_NO:
        sys.exit()
    else:
        print_virtual_lab_text(virtual_lab_reference.error_text)
        exit_virtual_lab()


def resume_virtual_lab():
    user_input = input_virtual_lab_text(VIRTUAL_LAB_RESUME)
    if user_input == VIRTUAL_LAB_YES:
        return True
    elif user_input == VIRTUAL_LAB_NO:
        return False
    else:
        print_virtual_lab_text(virtual_lab_reference.error_text)
        return resume_virtual_lab()


def end_virtual_lab():
    user_input = input_virtual_lab_text(VIRTUAL_LAB_RESTART)
    if user_input == VIRTUAL_LAB_YES:
        execute_virtual_lab_v2(virtual_lab_reference, True)
    elif user_input == VIRTUAL_LAB_NO:
        sys.exit()
    else:
        print_virtual_lab_text(virtual_lab_reference.error_text)
        end_virtual_lab()


def print_and_check_user_input(print_text, task=None, hint_index=None):
    user_input = input_virtual_lab_text(print_text)

    if virtual_lab_reference.valid_control(user_input):
        if not task and (user_input == virtual_lab_reference.control_solution or
                         user_input == virtual_lab_reference.control_hint):
            print_and_check_user_input(virtual_lab_reference.error_text, task, hint_index)
        if user_input == virtual_lab_reference.control_quit:
            exit_virtual_lab()
        if user_input == virtual_lab_reference.control_help:
            print_and_check_user_input(virtual_lab_reference.help_text, task, hint_index)
        if user_input == virtual_lab_reference.control_solution:
            if task:
                virtual_laboratory_statistic.handle_statistic(task, solution=True)
                print_and_check_user_input(task.solution, task, hint_index)
        if user_input == virtual_lab_reference.control_hint:
            if task:
                if hint_index > 0:
                    if hint_index > 1:
                        hint_index += 1
                        print_virtual_lab_text(task.hint_1)
                        print_and_check_user_input(task.hint_2, task, hint_index)
                    else:
                        hint_index += 1
                        virtual_laboratory_statistic.handle_statistic(task, hint_2=True)
                        print_and_check_user_input(task.hint_2, task, hint_index)
                else:
                    hint_index += 1
                    virtual_laboratory_statistic.handle_statistic(task, hint_1=True)
                    print_and_check_user_input(task.hint_1, task, hint_index)
    elif user_input == '':
        if task:
            print_and_check_user_input(virtual_lab_reference.hint_solution_text, task, hint_index)
        else:
            return
    else:
        if task:
            if is_input_solution(user_input, task.solution):
                print_virtual_lab_text(virtual_lab_reference.correct_solution_text)
                return
            else:
                virtual_laboratory_statistic.handle_statistic(task, wrong_answer=True)
                print_virtual_lab_text(virtual_lab_reference.error_text)
                print_and_check_user_input(virtual_lab_reference.hint_solution_text, task, hint_index)
        else:
            print_and_check_user_input(virtual_lab_reference.error_text, task, hint_index)


def handle_virtual_lab_structure_element(current_element, parent_element, loaded_element=False, print_end_text=True):
    if parent_element.get_next_sub_element(current_element):
        if current_element and not loaded_element and print_end_text:
            set_exit_flags(current_element, True)
            print_and_check_user_input(current_element.get_end_text())
        if not loaded_element:
            if is_virtual_lab_structure_element_empty(parent_element.get_next_sub_element(current_element)):
                tmp_element = parent_element.get_next_sub_element(current_element)
                return handle_virtual_lab_structure_element(tmp_element, parent_element, loaded_element, print_end_text)
            current_element = parent_element.get_next_sub_element(current_element)
            virtual_laboratory_statistic.handle_statistic(current_element)
        print_and_check_user_input(current_element.get_title())
        print_and_check_user_input(current_element.get_info_text())
        set_exit_flags(current_element, False)
        return current_element
    elif not parent_element.get_next_sub_element(current_element):
        if loaded_element:
            if is_virtual_lab_structure_element_empty(current_element):
                return handle_virtual_lab_structure_element(current_element, parent_element, loaded_element,
                                                            print_end_text)
            else:
                print_and_check_user_input(current_element.get_title())
                print_and_check_user_input(current_element.get_info_text())
                return current_element
        if current_element and current_element.get_end_text() and print_end_text:
            set_exit_flags(current_element, True)
            print_and_check_user_input(current_element.get_end_text())
            return None
        else:
            return None


def handle_task(parent_element, current_loaded_task=None, loaded_task=False, print_end_text=True):
    global current_task

    if loaded_task:
        current_task = current_loaded_task

    while True:
        hint_index = 0
        current_task = handle_virtual_lab_structure_element(current_task, parent_element, loaded_task, print_end_text)
        if current_task:
            print_and_check_user_input(current_task.exercise, current_task, hint_index)
        else:
            return
        if loaded_task:
            loaded_task = False


def execute_virtual_lab_v2(vl: virtual_laboratory_structure.VirtualLab, restart=False):
    global virtual_lab_reference
    virtual_lab_reference = vl
    print_virtual_lab_text(vl.welcome_text)

    next_topic = True
    loaded_topic = False
    loaded_sub_topic = False
    loaded_task = False
    global current_topic
    current_topic = None
    global current_sub_topic
    current_sub_topic = None
    global current_task
    current_task = None
    print_topic_end_text = True
    print_sub_topic_end_text = True
    print_task_end_text = True

    if not restart:
        if resume_virtual_lab():
            saved_virtual_lab = virtual_laboratory_read_load_save.load_virtual_lab()
            if saved_virtual_lab:
                if saved_virtual_lab[0] == 'LAST_TOPIC_COMPLETE_SHOW_END_TEXT':
                    current_topic = virtual_lab_reference.get_last_sub_element()
                    print_topic_end_text = False
                    set_exit_flags(current_topic, True)
                else:
                    current_topic = saved_virtual_lab[0]
                    if current_topic:
                        loaded_topic = True
                if saved_virtual_lab[1] == 'LAST_SUB_TOPIC_COMPLETE_SHOW_END_TEXT':
                    current_sub_topic = current_topic.get_last_sub_element()
                    print_sub_topic_end_text = False
                    set_exit_flags(current_sub_topic, True)
                else:
                    current_sub_topic = saved_virtual_lab[1]
                    if current_sub_topic:
                        loaded_sub_topic = True
                if saved_virtual_lab[2] == 'LAST_TASK_COMPLETE_SHOW_END_TEXT':
                    current_task = current_sub_topic.get_last_sub_element()
                    print_task_end_text = False
                    set_exit_flags(current_task, True)
                else:
                    current_task = saved_virtual_lab[2]
                    if current_task:
                        loaded_task = True
            else:
                print_virtual_lab_text(VIRTUAL_LAB_RESUME_ERROR)

    while True:
        if next_topic:
            if not print_topic_end_text:
                current_topic = handle_virtual_lab_structure_element(current_topic, vl, loaded_topic,
                                                                     print_topic_end_text)
                next_topic = False
            else:
                current_topic = handle_virtual_lab_structure_element(current_topic, vl, loaded_topic)
                next_topic = False
            if not current_topic:
                print_virtual_lab_text(virtual_laboratory_statistic.get_statistic())
                end_virtual_lab()

        if current_topic:
            if not print_sub_topic_end_text:
                current_sub_topic = handle_virtual_lab_structure_element(current_sub_topic, current_topic,
                                                                         loaded_sub_topic, print_sub_topic_end_text)
            else:
                current_sub_topic = handle_virtual_lab_structure_element(current_sub_topic, current_topic,
                                                                         loaded_sub_topic)
            if not current_sub_topic:
                next_topic = True

        if current_sub_topic:
            if not print_task_end_text:
                handle_task(current_sub_topic, current_task, loaded_task, print_task_end_text)
            else:
                handle_task(current_sub_topic, current_task, loaded_task)
        else:
            current_task = None

        if loaded_topic:
            loaded_topic = False
        if loaded_sub_topic:
            loaded_sub_topic = False
        if loaded_task:
            loaded_task = False
