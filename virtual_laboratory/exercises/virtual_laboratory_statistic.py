import virtual_laboratory_structure


class VirtualLabStatisticInterface:
    def add_sub_statistic_element(self, element):
        pass


class StatisticTopic(VirtualLabStatisticInterface):
    def __init__(self, topic_id, title):
        self.topic_id = topic_id
        self.title = title
        self.sub_topics = []

    def add_sub_statistic_element(self, element):
        self.sub_topics.append(element)


class StatisticSubTopic(VirtualLabStatisticInterface):
    def __init__(self, sub_topic_id, title):
        self.sub_topic_id = sub_topic_id
        self.title = title
        self.tasks = []

    def add_sub_statistic_element(self, element):
        self.tasks.append(element)


class StatisticTask(object):
    def __init__(self, task_id, title):
        self.task_id = task_id
        self.title = title
        self.number_of_wrong_answers = 0
        self.hint_1_queried = False
        self.hint_2_queried = False
        self.solution_queried = False


virtual_lab_statistics = []
statisticsTopic: StatisticTopic = None
statisticsSubTopic: StatisticSubTopic = None
statisticTask: StatisticTask = None


def get_virtual_lab_statistics_to_save():
    return virtual_lab_statistics


def set_virtual_lab_statistics_from_save(saved_virtual_lab_statistics):
    if saved_virtual_lab_statistics:
        global virtual_lab_statistics
        virtual_lab_statistics = saved_virtual_lab_statistics
        if virtual_lab_statistics and virtual_lab_statistics[-1]:
            global statisticsTopic
            statisticsTopic = virtual_lab_statistics[-1]
            if statisticsTopic and statisticsTopic.sub_topics and statisticsTopic.sub_topics[-1]:
                global statisticsSubTopic
                statisticsSubTopic = statisticsTopic.sub_topics[-1]
                if statisticsSubTopic and statisticsSubTopic.tasks and statisticsSubTopic.tasks[-1]:
                    global statisticTask
                    statisticTask = statisticsSubTopic.tasks[-1]


def handle_statistic(current_element, wrong_answer=False, hint_1=False, hint_2=False, solution=False):
    if isinstance(current_element, virtual_laboratory_structure.Topic):
        global statisticsTopic
        if not statisticsTopic or statisticsTopic.title != current_element.title:
            statisticsTopic = StatisticTopic(current_element.topic_id, current_element.title)
            virtual_lab_statistics.append(statisticsTopic)
        else:
            # error
            pass

    if isinstance(current_element, virtual_laboratory_structure.SubTopic):
        global statisticsSubTopic
        if not statisticsSubTopic or statisticsSubTopic.title != current_element.title:
            statisticsSubTopic = StatisticSubTopic(current_element.sub_topic_id, current_element.title)
            statisticsTopic.sub_topics.append(statisticsSubTopic)
        else:
            # error
            pass

    if isinstance(current_element, virtual_laboratory_structure.Task):
        global statisticTask
        if not statisticTask or statisticTask.title != current_element.title:
            statisticTask = StatisticTask(current_element.task_id, current_element.title)
            statisticsSubTopic.tasks.append(statisticTask)
        elif statisticTask and statisticTask.task_id == current_element.task_id:
            if wrong_answer:
                statisticTask.number_of_wrong_answers += 1
            if hint_1:
                statisticTask.hint_1_queried = True
            if hint_2:
                statisticTask.hint_2_queried = True
            if solution:
                statisticTask.solution_queried = True
        else:
            # error
            pass


def get_statistic():
    statistic_string = ''
    for topic in virtual_lab_statistics:
        statistic_string = statistic_string + 'Topic: ' + str(topic.title) + '\n'

        for sub_topic in topic.sub_topics:
            statistic_string = statistic_string + '\t' + 'Sub Topic: ' + str(sub_topic.title) + '\n'

            for task in sub_topic.tasks:
                statistic_string = statistic_string + '\t' + '\t' + 'Task: ' + str(task.title) + '\n'
                statistic_string = statistic_string + '\t' + '\t' + 'Number of wrong answers: ' + str(
                    task.number_of_wrong_answers) + '   ' + 'Hint one queried: ' + str(
                    task.hint_1_queried) + '   ' + 'Hint two queried: ' + str(
                    task.hint_2_queried) + '   ' + 'Solution queried: ' + str(
                    task.solution_queried) + '\n'
    return statistic_string
    # \t is the sequence for tab
    # \n is the sequence for new line
