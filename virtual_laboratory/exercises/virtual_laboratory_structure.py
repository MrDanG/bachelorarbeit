class VirtualLabStructureInterface:
    def get_id(self):
        pass

    def get_title(self):
        pass

    def get_info_text(self):
        pass

    def get_end_text(self):
        pass

    def add_sub_element(self, sub_element):
        pass

    def get_next_sub_element(self, current_sub_element=None):
        pass

    def get_last_sub_element(self):
        pass


class VirtualLab(VirtualLabStructureInterface):
    def __init__(self):
        self.welcome_text = ''
        self.help_text = ''
        self.error_text = ''
        self.hint_solution_text = ''
        self.correct_solution_text = ''
        self.control_help = 'help'
        self.control_quit = 'quit'
        self.control_hint = 'hint'
        self.control_solution = 'solution'
        self.topics = []

    def set_general_texts(self, welcome_text, help_text, error_text, hint_solution_text, correct_solution_text):
        self.welcome_text = welcome_text
        self.help_text = help_text
        self.error_text = error_text
        self.hint_solution_text = hint_solution_text
        self.correct_solution_text = correct_solution_text

    def valid_control(self, control):
        return control == self.control_help or \
               control == self.control_quit or \
               control == self.control_hint or \
               control == self.control_solution

    def add_sub_element(self, sub_element):
        self.topics.append(sub_element)

    def get_next_sub_element(self, current_sub_element=None):
        if current_sub_element is not None:
            for tmp_element in self.topics:
                if tmp_element.topic_id == current_sub_element.topic_id:
                    current_sub_element = tmp_element
            if self.topics.index(current_sub_element) < len(self.topics) - 1:
                return self.topics[self.topics.index(current_sub_element) + 1]
            else:
                return None
        else:
            if not self.topics:
                return None
            else:
                return self.topics[0]

    def get_last_sub_element(self):
        if self.topics:
            return self.topics[-1]
        else:
            return None


class Topic(VirtualLabStructureInterface):
    def __init__(self, topic_id, title, info_text, end_text):
        self.topic_id = topic_id
        self.title = title
        self.info_text = info_text
        self.end_text = end_text
        self.sub_topics = []

    def get_id(self):
        return self.topic_id

    def get_title(self):
        return self.title

    def get_info_text(self):
        return self.info_text

    def get_end_text(self):
        return self.end_text

    def add_sub_element(self, sub_element):
        self.sub_topics.append(sub_element)

    def get_next_sub_element(self, current_sub_element=None):
        if current_sub_element is not None:
            for tmp_element in self.sub_topics:
                if tmp_element.sub_topic_id == current_sub_element.sub_topic_id:
                    current_sub_element = tmp_element
            if self.sub_topics.index(current_sub_element) < len(self.sub_topics) - 1:
                return self.sub_topics[self.sub_topics.index(current_sub_element) + 1]
            else:
                return None
        else:
            if not self.sub_topics:
                return None
            else:
                return self.sub_topics[0]

    def get_last_sub_element(self):
        if self.sub_topics:
            return self.sub_topics[-1]
        else:
            return None


class SubTopic(VirtualLabStructureInterface):
    def __init__(self, sub_topic_id, title, info_text, end_text):
        self.sub_topic_id = sub_topic_id
        self.title = title
        self.info_text = info_text
        self.end_text = end_text
        self.tasks = []
        self.number_of_tasks = 0

    def get_id(self):
        return self.sub_topic_id

    def get_title(self):
        return self.title

    def get_info_text(self):
        return self.info_text

    def get_end_text(self):
        return self.end_text

    def add_sub_element(self, sub_element):
        self.tasks.append(sub_element)
        self.number_of_tasks += 1

    def get_next_sub_element(self, current_sub_element=None):
        if current_sub_element is not None:
            for tmp_element in self.tasks:
                if tmp_element.task_id == current_sub_element.task_id:
                    current_sub_element = tmp_element
            if self.tasks.index(current_sub_element) < len(self.tasks) - 1:
                return self.tasks[self.tasks.index(current_sub_element) + 1]
            else:
                return None
        else:
            if not self.tasks:
                return None
            else:
                return self.tasks[0]

    def get_last_sub_element(self):
        if self.tasks:
            return self.tasks[-1]
        else:
            return None


class Task(object):
    def __init__(self, task_id, title, info_text, exercise, solution, hint_1, hint_2, end_text):
        self.task_id = task_id
        self.title = title
        self.info_text = info_text
        self.exercise = exercise
        self.solution = solution
        self.hint_1 = hint_1
        self.hint_2 = hint_2
        self.end_text = end_text

    def get_id(self):
        return self.task_id

    def get_title(self):
        return self.title

    def get_info_text(self):
        return self.info_text

    def get_end_text(self):
        return self.end_text
